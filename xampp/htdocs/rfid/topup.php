<?php
session_start();
include 'connection.php';
include 'otoritas.php';	
if (!isset($_SESSION['loggedin'])) {
	header('Location: login.php');
	exit;
}

//=========[Ambil Password dari Logged In Operator]===========
$stmt = $con->prepare('SELECT password FROM tb_admin WHERE username = ?');
// In this case we can use the account ID to get the account info.
$stmt->bind_param('s', $_SESSION['name']);
$stmt->execute();
$stmt->bind_result($passwordcheck);
$stmt->fetch();
$stmt->close();

?>

<?php
if(isset($_POST['button3'])){
	$serialkey = $_POST['serialkey'];
	$id = $_POST['id'];
	$password = $_POST['password'];
	//Check validitas TopUp
	$ceksaldo = $con->prepare("SELECT  saldo FROM tb_user WHERE id=?");
	$ceksaldo->bind_param('s', $_POST['id']);
	$ceksaldo->execute();
	$ceksaldo->bind_result($saldo);
	$ceksaldo->fetch();
	$ceksaldo->close();

	//Check validitas ID Kartu
	$ceksaldo = $con->prepare("SELECT id FROM tb_user WHERE id=?");
	$ceksaldo->bind_param('s', $_POST['id']);
	$ceksaldo->execute();
	$ceksaldo->bind_result($idcheck);
	$ceksaldo->fetch();
	$ceksaldo->close();

	$serialtopup = $con->prepare("SELECT saldo FROM tb_topup WHERE serialkey = ?");
	$serialtopup->bind_param('s', $serialkey);
	$serialtopup->execute();
	$serialtopup->bind_result($saldotopup);
	$serialtopup->fetch();
	$serialtopup->close();
    if ($saldotopup < 1)
    {
    	echo "<script>alert('Kode Serial tidak ditemukan')</script>";
    }
    else if ($password != $passwordcheck)
    {
   	 	echo "<script>alert('Password Admin salah')</script>";
    }
    else if ($id != $idcheck)
    {
    	echo "<script>alert('ID Kartu tidak ditemukan')</script>";
    }
    else if ($saldotopup >= 1)
    {
    	$topup = "UPDATE tb_user SET saldo ='$saldo' + '$saldotopup' WHERE id='$id'";
    	$query = mysqli_query($con,$topup);
    	echo "<script>alert('Selamat, Top Up saldo sebesar Rp. $saldotopup berhasil ditambahkan')</script>";
    	$serialreset = "UPDATE tb_topup SET saldo = 0 WHERE serialkey= '$serialkey'";
		$query = mysqli_query($con, $serialreset);
	}	
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Top Up</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<meta charset="utf-8">
		<link href="style.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
	</head>
	<body class="loggedin">
		<nav class="navtop">
			<div>
				<h1>Toko Roti</h1>
				<a href="home.php"><i class="fas fa-home"></i>Home</a>
				<a href='transaksi.php'><i class='fas fa-shopping-cart'></i>Transaksi</a>
				<a href='users.php'><i class='fas fa-users-cog'></i>Users</a>
				<a href="profile.php"><i class="fas fa-user-circle"></i>Profile</a>
				<a href="topup.php"><i class="fas fa-cash-register"></i>Top Up</a>
				<a href="logout.php"><i class="fas fa-sign-out-alt"></i>Logout</a>
			</div>
			</div>
		</nav>
		<div class="content">
			<h2>Top Up</h2>
			<div>
			<form method="post">
				<table>
					<tr>
						<td colspan="3">TOP UP ACCOUNT</td>
					</tr>	
					<tr>
						<td>ID Kartu</td>
						<td>:</td>
						<td>
							<input type="text" name="id" placeholder="ID Kartu" id="id" required>
						</td>
					</tr>
					<tr>
						<td>Kode Voucher</td>
						<td>:</td>
						<td>
							<input type="text" name="serialkey" placeholder="Voucher" id="serialkey" required>
						</td>
					</tr>
					<tr>
						<td>Security Code</td>
						<td>:</td>
						<td>
							<input type="password" name="password" placeholder="password admin" id="password" required>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<button type="submit" name="button3" value="Top Up" class="btn btn-success">
								<i class="fas fa-coins"></i> <i class="fas fa-plus"></i>
								Top Up
							</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
		</div>

	<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h5><b>About</b></h5>
            <p class="text-justify">Website ini merupakan website prototype yang masih dalam proses pengembangan. Website ini berintegrasi dengan alat IOT yang menyediakan layanan pembayaran melalui <i>E-Money</i> menggunakan <i>RFID</i>. Website ini menggunakan layanan MySQL, Apache, PHP, HTML, Javascript, Ajax, dan Arduino.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h5><b>Contact</b></h5>
            <ul class="footer-links">
              <li>E-Mail : setiaidam@gmail.com </li>
              <li><a href="https://www.facebook.com/idm.stia/">Facebook</a></li>
              <li><a href="https://www.instagram.com/two2zero2/">Instagram</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h5><b>Alamat</b></h5>
            <p>
            	Mahar Martanegara no.48 Kota Cimahi, Jawa Barat
            </p>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
         <a href="#">IDAM SETIA</a>.
            </p>
          </div>
        </div>
      </div>
</footer>
	</body>
</html>