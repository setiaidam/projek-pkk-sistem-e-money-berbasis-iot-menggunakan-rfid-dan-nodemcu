<?php
session_start();
$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'rfid';
$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
if ( mysqli_connect_errno() ) {
	exit('Failed to connect to MySQL: ' . mysqli_connect_error());
}
if ( !isset($_POST['username'], $_POST['password']) ) {
	exit('Please fill both the username and password fields!');
}
// Menggunakan Prepared SQL sehingga menurunkan resiko SQL Injection
if ($stmt = $con->prepare('SELECT username, password FROM tb_admin WHERE username = ?')) {
	$stmt->bind_param('s', $_POST['username']);
	$stmt->execute();
	$stmt->store_result();
	if ($stmt->num_rows > 0) {
	$stmt->bind_result($username, $password);
	$stmt->fetch();
	// Verifikasi Password
	if ($_POST['password'] === $password) {
		// Verfifikasi Akun
		session_regenerate_id();
		$_SESSION['loggedin'] = TRUE;
		$_SESSION['name'] = $_POST['username'];
		$_SESSION['id'] = $username;
		echo 'Welcome ' . $_SESSION['name'] . '!';
		header("Location:home.php");
		exit();
	} else {
		// Password salah
		echo 'Incorrect username and/or password!';
	}
} else {
	// Username salah
	echo 'Incorrect username and/or password!';
}
	$stmt->close();
}
?>