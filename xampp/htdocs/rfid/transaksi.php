<?php
session_start();
include 'connection.php';
include 'otoritas.php';
if (!isset($_SESSION['loggedin'])) {
	header('Location: login.php');
	if ($auth == 'user'){
		header('Location: home.php');
					}
	else {
		header('Location: users.php');
	}
	exit;
}

?>

<html>
	<head>
		<title>PROTOTYPE | Sistem E-Money berbasis RFID</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<meta charset="utf-8">
		<link href="style.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
	</head>
	<body class="loggedin">
		<nav class="navtop">
			<div>
				<h1>Toko Roti</h1>
				<a href="home.php"><i class="fas fa-home"></i>Home</a>
				<a href='transaksi.php'><i class='fas fa-shopping-cart'></i>Transaksi</a>
				<a href='users.php'><i class='fas fa-users-cog'></i>Users</a>
				<a href="profile.php"><i class="fas fa-user-circle"></i>Profile</a>
				<a href="topup.php"><i class="fas fa-cash-register"></i>Top Up</a>
				<a href="logout.php"><i class="fas fa-sign-out-alt"></i>Logout</a>
			</div>
		</nav>
		<br/>
		<div class="content">
			<h2>Transaksi</h2>
			<br />
			<div class="table-responsive">
				<table class="table table-bordered">
					<tr>
						<th width="5%">Nomor</th>
						<th width="5%">ID Pelanggan</th>
						<th width="5%">Pelanggan</th>
						<th width="5%">Operator</th>
						<th width="5%">Subtotal</th>
						<th width="5%">Tanggal</th>
						<th width="1%">Keterangan</th>
					</tr>
					<?php
					$transaksi = "SELECT id, user_id, name, operator, subtotal, tanggal,status FROM transaksi";
					$result = $con->query($transaksi);
					if ($result->num_rows > 0) {
 				 			// output data of each row
 			 			while($row = $result->fetch_assoc())
 			 				{
					?>
					<tr>
						<td><?php echo $row["id"]; ?></td>
						<td><?php echo $row["user_id"]; ?></td>
						<td><?php echo $row["name"]; ?></td>
						<td><?php echo $row["operator"]; ?></td>
						<td>Rp. <?php echo $row["subtotal"]; ?></td>
						<td><?php echo $row["tanggal"]; ?></td>
						<td><?php echo $row["status"]; ?></td>
					</tr>
					<?php
						}
                          }
                          ?>
                    <tr>
						<td colspan="7" align="right">
							<form method="post">
                    		<?php                         
                            if(isset($_POST['export'])){
                                //and then execute a sql query here
                                $filename = "DataTransaksi.xls"; // File Name
								// Download file
								header("Content-Disposition: attachment; filename=\'$filename\'");
								header("Content-Type: application/vnd.ms-excel");
								$user_query = mysql_query('select * from transaksi');
								// Write data to file
								$flag = false;
								while ($row = mysql_fetch_assoc($user_query)) {
								    if (!$flag) {
								        // display field/column names as first row
								        echo implode("\t", array_keys($row)) . "\r\n";
								        $flag = true;
								    }
								    echo implode("\t", array_values($row)) . "\r\n";
									}
                           		}
                   			 ?>
                                <button type="submit" name="export" style="margin-top:5px;" class="btn btn-success" value="Export">
                                <i class="fas fa-download"></i> Download
                                </button>
                        </form>
						</td>
					</tr>  
				</table>
			</div>
		</div>
	</div>
	<br />
<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h5><b>About</b></h5>
            <p class="text-justify">Website ini merupakan website prototype yang masih dalam proses pengembangan. Website ini berintegrasi dengan alat IOT yang menyediakan layanan pembayaran melalui <i>E-Money</i> menggunakan <i>RFID</i>. Website ini menggunakan layanan MySQL, Apache, PHP, HTML, Javascript, Ajax, dan Arduino.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h5><b>Contact</b></h5>
            <ul class="footer-links">
              <li>E-Mail : setiaidam@gmail.com </li>
              <li><a href="https://www.facebook.com/idm.stia/">Facebook</a></li>
              <li><a href="https://www.instagram.com/two2zero2/">Instagram</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h5><b>Alamat</b></h5>
            <p>
            	Mahar Martanegara no.48 Kota Cimahi, Jawa Barat
            </p>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
         <a href="#">IDAM SETIA</a>.
            </p>
          </div>
        </div>
      </div>
</footer>
</body>
</html>