<?php
// We need to use sessions, so you should always start sessions using the below code.
session_start();
// If the user is not logged in redirect to the login page...
if (!isset($_SESSION['loggedin'])) {
	header('Location: login.php');
	exit;
}
include "connection.php";
include 'otoritas.php';	
// We don't have the password or email info stored in sessions so instead we can get the results from the database.
$stmt = $con->prepare('SELECT admin_name, username FROM tb_admin WHERE username = ?');
// In this case we can use the account ID to get the account info.
$stmt->bind_param('s', $_SESSION['name']);
$stmt->execute();
$stmt->bind_result($nama, $username);
$stmt->fetch();
$stmt->close();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Profile Page</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<meta charset="utf-8">
		<link href="style.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
	</head>
	<body class="loggedin">
		<nav class="navtop">
			<div>
				<h1>Toko Roti</h1>
				<a href="home.php"><i class="fas fa-home"></i>Home</a>
				<a href='transaksi.php'><i class='fas fa-shopping-cart'></i>Transaksi</a>
				<a href='users.php'><i class='fas fa-users-cog'></i>Users</a>
				<a href="profile.php"><i class="fas fa-user-circle"></i>Profile</a>
				<a href="topup.php"><i class="fas fa-cash-register"></i>Top Up</a>
				<a href="logout.php"><i class="fas fa-sign-out-alt"></i>Logout</a>
			</div>
		</nav>
		<div class="content">
			<h2>Profile Page</h2>
			<div>
				<p>Your account details are below:</p>
				<table>
					<tr>
						<td>Operator</td>
						<td>:</td>
						<td><?=$nama?></td>
					</tr>
					<tr>
						<td>Username</td>
						<td>:</td>
						<td><?=$username?></td>
					</tr>
				</table>
			</div>
		</div>

		<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h5><b>About</b></h5>
            <p class="text-justify">Website ini merupakan website prototype yang masih dalam proses pengembangan. Website ini berintegrasi dengan alat IOT yang menyediakan layanan pembayaran melalui <i>E-Money</i> menggunakan <i>RFID</i>. Website ini menggunakan layanan MySQL, Apache, PHP, HTML, Javascript, Ajax, dan Arduino.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h5><b>Contact</b></h5>
            <ul class="footer-links">
              <li>E-Mail : setiaidam@gmail.com </li>
              <li><a href="https://www.facebook.com/idm.stia/">Facebook</a></li>
              <li><a href="https://www.instagram.com/two2zero2/">Instagram</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h5><b>Alamat</b></h5>
            <p>
            	Mahar Martanegara no.48 Kota Cimahi, Jawa Barat
            </p>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
         <a href="#">IDAM SETIA</a>.
            </p>
          </div>
        </div>
      </div>
</footer>
		
	</body>
</html>