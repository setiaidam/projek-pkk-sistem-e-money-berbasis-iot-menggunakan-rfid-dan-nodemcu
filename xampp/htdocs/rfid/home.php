<?php 
session_start();
$connect = mysqli_connect("localhost", "root", "", "rfid");
$date = date('Y-m-d H:i:s');   
if (!isset($_SESSION['loggedin'])) {
	header('Location: login.php');
	exit;
}
include 'otoritas.php';		

if(isset($_POST["add_to_cart"]))
{
	if(isset($_SESSION["shopping_cart"]))
	{
		$item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
		if(!in_array($_GET["id"], $item_array_id))
		{
			$count = count($_SESSION["shopping_cart"]);
			$item_array = array(
				'item_id'			=>	$_GET["id"],
				'item_name'			=>	$_POST["hidden_name"],
				'item_price'		=>	$_POST["hidden_price"],
				'item_quantity'		=>	$_POST["quantity"]
			);
			$_SESSION["shopping_cart"][$count] = $item_array;
		}
		else
		{
			echo '<script>alert("Item Already Added")</script>';
		}
	}
	else
	{
		$item_array = array(
			'item_id'			=>	$_GET["id"],
			'item_name'			=>	$_POST["hidden_name"],
			'item_price'		=>	$_POST["hidden_price"],
			'item_quantity'		=>	$_POST["quantity"]
		);
		$_SESSION["shopping_cart"][0] = $item_array;
	}
}

if(isset($_GET["action"]))
{
	if($_GET["action"] == "delete")
	{
		foreach($_SESSION["shopping_cart"] as $keys => $values)
		{
			if($values["item_id"] == $_GET["id"])
			{
				unset($_SESSION["shopping_cart"][$keys]);
				echo '<script>alert("Item Removed")</script>';
				echo '<script>window.location="home.php"</script>';
			}
		}
	}
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>PROTOTYPE | Sistem E-Money berbasis RFID</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<meta charset="utf-8">
		<link href="style.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
	</head>
	<body class="loggedin">
		<nav class="navtop">
			<div>
				<h1>Toko Roti</h1>
				<a href="home.php"><i class="fas fa-home"></i>Home</a>
				<a href='transaksi.php'><i class='fas fa-shopping-cart'></i>Transaksi</a>
				<a href='users.php'><i class='fas fa-users-cog'></i>Users</a>
				<a href="profile.php"><i class="fas fa-user-circle"></i>Profile</a>
				<a href="topup.php"><i class="fas fa-cash-register"></i>Top Up</a>
				<a href="logout.php"><i class="fas fa-sign-out-alt"></i>Logout</a>
			</div>
		</nav>
		<br/>
		<div class="content">
				<h2>PROTOTYPE E-Money Berbasis RFID</h2>
			<?php
				$query = "SELECT * FROM tbl_product ORDER BY id ASC";
				$result = mysqli_query($connect, $query);
				if(mysqli_num_rows($result) > 0)
				{
					while($row = mysqli_fetch_array($result))
					{
				?>
			<div class="col-md-4">
				<form method="post" action="home.php?action=add&id=<?php echo $row["id"]; ?>">
					<div style="border:1px solid #333; background-color:#ffffff; border-radius:5px; padding:16px;" align="center">
						<img src="images/<?php echo $row["image"]; ?>" class="img-responsive" /><br />

						<h4 class="text-info"><?php echo $row["name"]; ?></h4>

						<h4 class="text-danger">Rp. <?php echo $row["price"]; ?></h4>

						<input type="text" name="quantity" value="1" class="form-control" />

						<input type="hidden" name="hidden_name" value="<?php echo $row["name"]; ?>" />

						<input type="hidden" name="hidden_price" value="<?php echo $row["price"]; ?>" />

						<input type="submit" name="add_to_cart" style="margin-top:5px;" class="btn btn-success" value="Beli" />

					</div>
				</form>
			</div>
			<?php
					}
				}
			?>
			<!-- <div style="clear:both"></div> -->
			<br />
			<h3>Order Details</h3>
			<div class="table-responsive">
				<table class="table table-bordered">
					<tr>
						<th width="40%">Barang</th>
						<th width="10%">Jumlah</th>
						<th width="20%">Harga</th>
						<th width="15%">Total</th>
						<th width="5%">Action</th>
					</tr>
					<?php
					if(!empty($_SESSION["shopping_cart"]))
					{
						$total = 0;
						foreach($_SESSION["shopping_cart"] as $keys => $values)
						{
					?>
					<tr>
						<td><?php echo $values["item_name"]; ?></td>
						<td><?php echo $values["item_quantity"]; ?></td>
						<td>Rp. <?php echo $values["item_price"]; ?></td>
						<td>Rp. <?php echo number_format($values["item_quantity"] * $values["item_price"], 2);?></td>
						<td>
							<button class="btn btn-success" onclick="location.href='home.php?action=delete&id=<?php echo $values["item_id"]; ?>'">
								<i class="fas fa-trash"></i>
							<!-- <a href="home.php?action=delete&id=<?php //echo $values["item_id"]; ?>"></a></td> -->
							</button>
					</tr>
					<?php
							$total = $total + ($values["item_quantity"] * $values["item_price"]);
						}
					?>
					<tr>
						<td colspan="3" align="right">Total</td>
						<td align="right">Rp. <?php echo number_format($total, 2); ?></td>
						<?php?>
						
                        <form method="post">
                    <?php                         
                            if(isset($_POST['button1'])){
                                //and then execute a sql query here
                                include 'connection.php';
                                $transaksi_admin = "INSERT INTO transaksi(user_id, operator, subtotal, tanggal, status) VALUES('0','$pengenal','$total','$date','BAYAR')";
                                $query = mysqli_query($con,$transaksi_admin);
                                echo '<script>alert("Item telah Dipesan !")</script>';
			
                            }
                            if(isset($_POST['button2'])){
                                //and then execute a sql query here
                                include 'connection.php';
                                unset($_SESSION['shopping_cart']);
                                echo '<script>alert("Item telah di Reset !")</script>';
                                echo "<script> location.href='home.php'; </script>";

			
                            }
                    ?>
                            <td align="right">
                                <button type="submit" name="button1" style="margin-top:5px;" class="btn btn-success" value="Pesan">
                                <i class="fas fa-cart-plus"></i>
                                </button>
                                <button type="submit" name="button2" style="margin-top:5px;" class="btn btn-success" value="Reset">
                                <i class="fas fa-undo"></i>
                                </button>
                            </td>
                        </form>
					</tr>
                    <?php 
                          }
                          ?>  
				</table>
			</div>
		</div>
	</div>
	<br />
	
	<footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h5><b>About</b></h5>
            <p class="text-justify">Website ini merupakan website prototype yang masih dalam proses pengembangan. Website ini berintegrasi dengan alat IOT yang menyediakan layanan pembayaran melalui <i>E-Money</i> menggunakan <i>RFID</i>. Website ini menggunakan layanan MySQL, Apache, PHP, HTML, Javascript, Ajax, dan Arduino.</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h5><b>Contact</b></h5>
            <ul class="footer-links">
              <li>E-Mail : setiaidam@gmail.com </li>
              <li><a href="https://www.facebook.com/idm.stia/">Facebook</a></li>
              <li><a href="https://www.instagram.com/two2zero2/">Instagram</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h5><b>Alamat</b></h5>
            <p>
            	Mahar Martanegara no.48 Kota Cimahi, Jawa Barat
            </p>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2021 All Rights Reserved by 
         <a href="#">IDAM SETIA</a>.
            </p>
          </div>
        </div>
      </div>
</footer>

	</body>
</html>

<?php
//If you have use Older PHP Version, Please Uncomment this function for removing error 

/*function array_column($array, $column_name)
{
	$output = array();
	foreach($array as $keys => $values)
	{
		$output[] = $values[$column_name];
	}
	return $output;
}*/
?>