<?php
include "connection.php";
	// Setting pengenal transaksi (User Id)
	$cekuid = $con->prepare("SELECT id FROM tb_user WHERE username = ?");
	$cekuid->bind_param('s', $_SESSION['name']);
	$cekuid->execute();
	$cekuid->bind_result($userid);
	$cekuid->fetch();
	$cekuid->close();
	// Setting pengenal transaksi (Nama)
	$cekpengenal = $con->prepare("SELECT admin_name FROM tb_admin WHERE username = ?");
	$cekpengenal->bind_param('s', $_SESSION['name']);
	$cekpengenal->execute();
	$cekpengenal->bind_result($pengenal);
	$cekpengenal->fetch();
	$cekpengenal->close();
?>